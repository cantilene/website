---
title: "Dirigent"
menu:
  main:
    weight: 30
---

## Onze dirigent: Peggy De Schrijver

Muziek is bij Peggy steeds een grote hobby geweest: ze volgde aan de toenmalige Gemeentelijke Muziekschool van Merelbeke piano, orgel en cornet resp. bij Michaël De Permentier, Paul Hoste en Rudy Reunes.

Op vraag van pastoor De Bruyne is ze sinds haar twaalfde één van de organisten in de kerk van Melsen.
De passie van deze pastoor voor zowel klavier- als koormuziek heeft ook Peggy niet onberoerd gelaten, want later ging ze zich meer toeleggen op zingen bij amateurkoren.

Vanuit deze achtergrond en de nood aan een koor voor kinderen die graag zingen, richtte ze in 2015 [Jeugdkoor Melsen](https://sites.google.com/view/jeugdkoormelsen/) op, waarvan ze tevens dirigent is.
Gaandeweg volgde ze ook diverse vormingen bij Koor&Stem rond alles wat zingen met kinderen aangaat.

Bij koor Cantilene was Peggy reeds jaar en dag een vaste waarde bij de sopranen.
In 2022 nam ze met veel enthousiasme het dirigeerstokje bij Cantilene over van Geert.
