---
title: "Repetities"
menu:
  main:
    weight: 50
---
# Repetitielocatie
Onze repetities gaan door elke donderdagavond van 19.45u tot 21.45u.
Wij zingen in de Sint-Hendrikskerk in de Sint-Elooistraat 116 te 9820 Merelbeke.

<div class="uk-child-width-expand@m" uk-grid>
  <div>
  {{< figure src="/images/kerkkwenenbos.jpg" >}}
  </div>
  <div>
  <iframe width="425" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=3.7240862846374516%2C50.977112002274545%2C3.728045225143433%2C50.980043668912714&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/50.97858/3.72607">Grotere kaart bekijken</a></small>
  </div>
</div>
