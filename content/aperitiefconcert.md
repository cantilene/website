---
title: "Aperitiefconcert"
menu:
  main:
    weight: 15
draft: true
---

# Over het leven en de liefde

Op zondag 25 februari 2024 om 10.30 uur presenteert koor Cantilene een Aperitiefconcert getiteld _Over het leven en de liefde_.

We krijgen gezelschap van Jeugdkoor Melsen.
Michaël De Permentier begeleidt ons op piano.
Het geheel wordt gepresenteerd door Marleen Vande Maele.

_Het concert is volledig uitverkocht._  
~~Bestel uw kaarten in voorverkoop aan € 10 per persoon via <koorcantilene@gmail.com>.
Jonger dan 12 jaar? Dan mag je gratis komen luisteren.~~

Geniet nadien met ons mee van een gratis drankje.

{{< figure src="/images/affiche_2024.png" >}}
