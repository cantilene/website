---
title: "Historiek"
menu:
  main:
    weight: 40
---
## Hoe het groeide

De wijk Kwenenbos ligt ten zuiden van de gemeente Merelbeke.
Daar staat een gezellig wit kerkje.
In de jaren 80 was Frans Hendrickx daar de onderpastoor.
Tevens was hij de aalmoezenier van scoutsgroep De Graalridders.
Het scoutskoor o.l.v. groepsleider Geert Kint was jarenlang het gelegenheidskoor in de Sint-Hendrikskerk.
Stilaan groeide het idee om een vast koor op te richten, met wekelijkse repetities.
Het was de bedoeling om de hoogdagen op te luisteren.
En zie, op 3 april 1988 luisteren 8 enthousiaste zangers de paasmis in de Sint-Hendrikskerk op.
Stilaan kwamen meer zangers aansluiten.
En er werd een naam gezocht: _Cantilene_.

Het koor bracht eigentijdse muziek: religieus en profaan.
Op 21 december 1991 organiseerde Cantilene zijn eerste kerstconcert, samen met het mannenkoor Singhet Scone (Gent), hoboïst Joris Van de Hauwe en organist Wilfried van Baelen.
Cantilene, een vriendengroep van gemotiveerde zangers, besloot in 1992 mee te doen aan de Provinciale Koorzangtoernooien in het Conservatorium te Gent.
 Het koor werd gerangschikt in Tweede divisie. Het resultaat van hard werken.

{{< figure src="/images/cantilene-1992-met-cijfertjes.jpg" class="uk-align-right" caption="Provinciaal Koorzangtoernooi 1992" >}}

## Cantilene 1992: Conservatorium Gent

1 Monique Van Loocke -
2 Marie-Louise Bogaert -
3 Mieke De Loore -
4 Marita De Temmerman -
5 Marie-Christine Landsgraeve -
6 Katrien De Vleesschouwer -
7 Katrien Dhondt -
8 Roza Van den Bulcke -
9 Helena Van de Velde -
10 Monique Steenhoudt -
11 Ann De Medts -
12 Agnes Van den Bulcke -
13 Hilda Van den Bulcke -
14 Marie-Paule Declercq -
15 Rosanne Van Ceulebroeck -
16 Wilfried Weyts -
17 André De Bruyne -
18 Marc Lootens -
19 Antoine De Waele -
20 Antoine De Schepper -
21 Norbert Kint -
22 Roger Verbeke -
23 Marcel Boone -
24 Daniël Wollaert -
25 Geert Kint

Ondertussen gaf Cantilene optredens op verschillende locaties, in de buurt en ver daarbuiten.
Het koor liet zich horen in Veurne, Lissewege, Oostduinkerke, Han-sur-Lesse, Zoutleeuw.
Cantilene werd gevraagd om een benefiet-kerstconcert te geven ten voordele van DVC Heilig Hart Deinze in de prachtige kerk van Deurle. Een moment om niet meer te vergeten.
In 1997 werd Cantilene aangezocht om een concert te geven in de Sint-Pauluskerk in Deinze. Het werd echt iets speciaals. Het koor werd begeleid door Jan Vuye op orgel en het Flanders Trumpet Choir met Rudy Reunes en Carry De Clercq.

Ondertussen moest het koor om de vier jaar deelnemen aan de Provinciale Koorzangtoernooien.
Telkens werd het bevestigd met een "stevige Tweede Divisie".
Stilaan kwam de eeuwwende in zicht.
Dat moest iets speciaal worden.
Koorleider Geert vond een partituur "Die Weihnachtgeschichte" van Max Drishner.
Samen met het koor Pazako uit Destelbergen, een bariton-solist, een pianist en een strijkkwartet werden in de kerstperiode twee prachtige concerten gegeven.
Eentje in De Kwenenboskerk en eentje in de Pius-X-kerk. Een mijlpaal voor het koor.

Enkele maanden later rangschikte Cantilene zich op het Provinciaal Koorzangtoernooi 2000 in Tweede Divisie.
De kroon op een drukke periode.
In januari 2003 nam Guido Vanhoucke het dirigeerstokje over.
Het koor repeteerde vanaf dan om de 14 dagen.
Het Cantilene legde zich toe op geestelijke liederen en op de hoogdagen werden de missen gezongen in de Sint-Hendrikskerk.
Cantilene werd uitgenodigd om een dankmis te zingen tgv. het einde van de herstellingswerken aan de kerk van Melsen.
Daar werd de volledige "Messe Brève n° 7" van Charles Gounod gezongen.
Voor de bewoners van het OCMW-rusthuis werd jaarlijks een kerstprogramma gebracht.
Dit is nog steeds een jaarlijks evenement.
Vanuit de verzorgingsinstelling Caritas-Melle werd een TV-mis uitgezonden waarbij Cantilene de koorzang verzorgde.
In 2012 stond een kerstconcert op het programma. In Koksijde werd een gastopreden gegeven.
In 2016 besloot Guido Vanhoucke te stoppen met dirigeren.
Zijn dirigentschap eindigde met een [winterconcert]({{< ref "20161211" >}}).
In januari 2017 nam Geert Kint het koor opnieuw onder zijn hoede.

De Sint-Hendrikskerk in Kwenenbos werd ondertussen gesloten voor de eredienst.
Voor het 30-jarige bestaan van het koor werd gekozen om een speciaal optreden in elkaar te zetten.
["Er was eens..."]({{< ref "20191222" >}}) werd een concert met een rode-draad-verhaal, het koor Cantilene, pianist Lodewijk Mispelon en een strijkkwartet.
De 30-jarige verjaardag werd een heel speciaal optreden !

Kort na het optreden voor het 30-jarige bestaan van het koor brak de COVID-pandemie uit.
Zoals in de rest van de maatschappij werd ook de werking van het koor noodgedwongen opgeschort.
Zodra het weer kon begonnen we opnieuw te repeteren met respect voor de toen geldende beperkingen.

In 2022 nam Peggy De Schrijver het dirigeerstokje over van Geert.
Onder haar enthousiaste leiding namen we de draad weer op.
Intussen mocht Cantilene ook enkele nieuwe leden verwelkomen.
We werden terug een bloeiend koor met gedreven leden en medewerkers.

Nieuwe leden blijven welkom en kunnen [contact opnemen]({{< ref "contact.md" >}}) om vrijblijvend van een repetitie te proeven.
