---
title: "Contact"
menu:
  main:
    weight: 70
side_image: 
  - /images/telefoon2.jpg
  - /images/typewriter.jpg
---
## Contact

Had je graag meer info, wil je graag meezingen, heb je een andere vraag?
Vul dan onderstaand formuliertje in en je vraag komt terecht bij het bestuur.
Je mag ook een email sturen naar [koorcantilene@gmail.com](mailto:koorcantilene@gmail.com).

| Bestuur        |                         |               |
| ---            | ---                     | ---           |
| Voorzitster    | Rosanne Van Ceulebroeck | 09/230.55.83  |
| Secretaris     | Daniël Wollaert         | 09/362.72.69  |
| Schatbewaarder | Patrick Danneels        | 09/324.69.68  |
| Bestuurslid    | Bart Brosens            | 0485/51.29.18 |
{.uk-table .uk-table-divider .uk-table-hover}

{{< netlify-contact-form >}}
