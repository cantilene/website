---
title: "Home"
menu:
  main:
    weight: 10
---

## Koor Cantilene Merelbeke

Cantilene is een gemengd koor uit Merelbeke.
We repeteren op donderdagavond in de parochiezaal van Merelbeke Kwenenbos.

[**De foto's van het aperitiefconcert staan online.**](/fotos/20240225/)

Op deze website kunt u alles lezen over ons koor, de repetities, hoe het groeide, de concerten en de dirigent.  
Heeft u vragen, neem dan contact op via de [contactpagina]({{< ref "contact" >}}).

<div uk-grid>
    <div class="uk-width-1-6@m uk-margin">
        <center>
            <img src="images/koorlogo.jpg">
        </center>
        <div class="uk-text-center uk-margin-top">
            Klik zeker eens door naar de <a href="/fotos">foto's</a>
        </div>
    </div>
    <div class="uk-width-5-6@m">
        <img src="images/hele_koor_1000.jpg">
    </div>
</div>
