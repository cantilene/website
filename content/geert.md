---
title: "Geert"
draft: true
---
## Geert KINT, oprichter en voormalig dirigent

{{< figure src="/images/geert-kint_orig.jpg" class="uk-align-right" width="275" >}}
Geert, van opleiding onderwijzer, is een muzikale duizendpoot.

Voor kinderen schreef hij verschillende klas- en schoolmusicals.
Hij volgde de cursus orgel bij Alfons Volckaert in Merelbeke.
Die gaf hem ook lessen in praktische harmonie en begeleiding van koren.
Daarnaast volgde Geert ook de cursussen _harmonie_ en _dictie-voordracht_.

In de plaatselijke scoutsgroep was Geert met zijn gitaar overal aanwezig.
Daar heeft hij eind jaren 70 het Graalridderskoor opgericht.
Met 40 zangers en 8 muzikanten (leerkrachten muziekschool) heeft het koor jarenlang allerlei vieringen opgeluisterd.
Geert volgde 3 jaar de weekendcursussen _koordirectie_ waar
hij les kreeg van de professoren Michaël Sheck, Juliaan Wilmots en Rudi Tas.

In april 1988 richtte Geert het gemend koor Cantilene op.
Cantilene stond tot 2003 onder zijn leiding.
Ondertussen richtte hij het jeugdkoor Alegria op.
Hiermee heeft hij veel concerten gegeven, met als hoogtepunt een TV-mis op de VRT, samen het kamerorkest The Symfunietta o.l.v. Rudi Reunes.

In 2010 startte Geert met historische opzoekingen naar het frontleven van zijn grootvader, Remi Bogaert.
Dit resulteerde in 2014 in een [theatermonoloog/multimediavoorstelling](https://www.vertelselsuitdenoorlog.be/) waarin hijzelf de rol van Remi vertolkte.

Van januari 2017 tot 2022 nam Geert met veel enthousiasme opnieuw het dirigeerstokje in de hand.
