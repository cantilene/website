---
title: "Koor"
menu:
  main:
    weight: 20
---
## Cantilene, een tof koor

{{< figure src="/images/koor_201912.jpg" class="uk-align-left" width="450" >}}
Het gemengd koor Cantilene werd opgericht in 1987.

Ooit begonnen we als een koor dat de eucharistievieringen in de Sint-Hendrikskerk opluisterde.
Maar ondertussen zijn we geëvolueerd naar een koor dat naast religieuze muziek ook meer en meer profaan werk zingt.
Cantilene probeert daarbij een goede middenweg te zoeken.

Alle muziekstijlen komen aan bod: eigentijdse moderne koormuziek, polyfonie, gospels - Nederlandstalig maar ook Frans, Engels, Pools, Spaans, ...
We zingen A Capella, met piano- of orgebegeleiding en met solisten-instrumentisten.

We zijn een enthousiaste groep gemotiveerde zangers die  steeds het beste van zichzelf geven, zowel op de repetities als op de uitvoeringen.
De repetities verlopen steeds in een gemoedelijke sfeer.
Ook al zijn we geen beroepsmuzikanten, toch streven we ernaar om correct te zingen, met oog voor een mooie interpretatie.
We proberen, stap voor stap, ons niveau te verfijnen.
Ons handelsmerk is het brengen van een warm en fijn optreden, met zin voor detail en afwerking.
Maar zo nu en dan trekken we er samen op uit, gaan we eens tafelen, houden we eens een "zingdag".
Er is altijd wel iets te beleven.

Kom je meezingen met onze sopranen, alten, tenoren en bassen?
Wij zullen je warm verwelkomen !!

<div class="uk-child-width-expand@m uk-padding uk-padding-remove-horizontal uk-flex-center" uk-grid>
    <div class="uk-width-1-3">
    {{< figure src="/images/winterconcert-6.jpg" >}}
    </div>
    <div class="uk-width-1-3">
    {{< figure src="/images/winterconcert-23.jpg" >}}
    </div>
    <div class="uk-width-1-3">
    {{< figure src="/images/img-1442.jpg" >}}
    </div>
    <div class="uk-width-1-3">
    {{< figure src="/images/img-20190630-134301139.jpg" >}}
    </div>
    <div class="uk-width-2-3">
    {{< figure src="/images/img-9924.jpg" >}}
    </div>
</div>
