---
title: "Sint Stefanus Melsen 29 december 2024"
date: 2024-12-29
---

Parochiale viering in de kerk van Melsen op 29 december 2024 verzorgd door Cantilene en JKM.

Foto's door Marleen Vande Maele en Marc Van Hove.
