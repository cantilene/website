---
title: "Kerstconcert `Er Was eens...` 22 december 2019"
date: 2019-12-22
---

Op 22 december werd het concert **Er was eens...** opgevoerd in de centrumkerk te Merelbeke.
Koor Cantilene werd muzikaal ondersteund door Lodewijk Mispelon op het orgel en strijkers-kwartet
Capricornus-ensemble.
Franciska Danneels zorgde voor de vertelling.

De foto's hieronder werden gemaakt door Anneke Deneve.
