---
title: "Aperitiefconcert 2024"
date: 2024-02-25
---

Op zondag 25 februari 2024 presenteerde koor Cantilene een Aperitiefconcert getiteld _Over het leven en de liefde_.

We kregen gezelschap van Jeugdkoor Melsen.
Michaël De Permentier begeleidde ons op piano.
Het geheel werd gepresenteerd door Marleen Vande Maele en kleindochter Jynthe.

{{< figure src="/images/banner_2024.png" >}}
