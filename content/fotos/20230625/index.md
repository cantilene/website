---
title: "KoorBBQ 25 juni 2023"
date: 2023-06-25
---

Op 25 juni 2023 hielden we onze traditionele, jaarlijkse BBQ voor koorleden en hun gezin.
Vooraf was er een aperitiefconcert voor de gasten.
