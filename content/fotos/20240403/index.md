---
title: "Optreden Zelzate 3 april 2024"
date: 2024-04-03
---

Op woensdag 3 april brachten we ons repertoire in WZC Zilverbos te Zelzate.

Michaël De Permentier begeleidde ons op piano.

Hieronder enkele sfeerbeelden van het optreden.
