---
title: "Optredens kerstperiode 2023-2024"
date: 2024-01-07
---

De voorbije kerstperiode verzorgden we op kerstdag de mis in Oosterzele en traden we op voor de mensen van _WZC Berkenhof_ in Merelbeke en in _WZC Sint Elisabeth_ te Oostakker.
Hierbij enkele sfeerbeelden.
